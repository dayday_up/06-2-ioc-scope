package com.twuc.webApp.yourTurn;

import com.twuc.webApp.Interface.InterfaceOne;
import com.twuc.webApp.InterfaceOneImpl;
import com.twuc.webApp.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class AnnotationIocTest {

    private AnnotationConfigApplicationContext context;

    @BeforeEach
    void setUp() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
    }

    @Test
    void should_return_same_object_when_implements_interface() {
        InterfaceOneImpl interfaceOneImpl = context.getBean(InterfaceOneImpl.class);
        InterfaceOne interfaceOne = context.getBean(InterfaceOne.class);

        assertSame(interfaceOne, interfaceOneImpl);
    }

    @Test
    void should_return_same_object_when_extend_super() {
        SubObject subObject = context.getBean(SubObject.class);
        SuperObject superObject = context.getBean(SuperObject.class);

        assertEquals(subObject, superObject);
    }

    @Test
    void should_return_same_object_when_extends_abstract_class() {
        DerivedClass derivedClass = context.getBean(DerivedClass.class);
        AbstractBaseClass abstractBaseClass = context.getBean(AbstractBaseClass.class);
        assertEquals(derivedClass, abstractBaseClass);
    }

    @Test
    void should_return_not_same_object_when_use_prototype_scope() {
        SimplePrototypeScopeClass simplePrototypeScopeClass1 = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass simplePrototypeScopeClass2 = context.getBean(SimplePrototypeScopeClass.class);
        assertNotEquals(simplePrototypeScopeClass1, simplePrototypeScopeClass2);
    }

    @Test
    void should_return_new_instance_when_get_bean() {
        Prototype bean = context.getBean(Prototype.class);
        Prototype anotherBean = context.getBean(Prototype.class);
        assertNotSame(bean, anotherBean);
        List<String> logs = bean.getLogger().getLogs();
        assertEquals(2, logs.stream().filter(log -> log.equals("prototype()")).count());
    }

    @Test
    void should_create_two_prototype_and_one_singleton_when_prototype_dependency_singleton() {
        PrototypeScopeDependsOnSingleton prototype = context.getBean(PrototypeScopeDependsOnSingleton.class);
        context.getBean(PrototypeScopeDependsOnSingleton.class);
        List<String> logs = prototype.getMyLogger().getLogs();
        assertEquals(1, logs.stream().filter(log -> log.equals("singleton")).count());
        assertEquals(2, logs.stream().filter(log -> log.equals("prototype")).count());
    }

    @Test
    void should_create_one_prototype_and_one_singleton_when_singleton_dependency_prototype() {
        SingletonDependsOnPrototype prototype = context.getBean(SingletonDependsOnPrototype.class);
        context.getBean(SingletonDependsOnPrototype.class);
        List<String> logs = prototype.getMyLogger().getLogs();
        assertEquals(1, logs.stream().filter(log -> log.equals("singletonDependsOnPrototype()")).count());
        assertEquals(1, logs.stream().filter(log -> log.equals("prototypeDependent()")).count());
    }

    @Test
    void should_create_multiple_prototype_object_when_singleton_dependency_prototype() {
        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);
        context.getBean(SingletonDependsOnPrototype.class);
        List<String> logs = bean.getPrototype().getLogger().getLogs();
        assertEquals(1, logs.stream().filter(log -> log.equals("PrototypeDependentWithProxy")).count());
        bean.invokeMethod();
        assertEquals(2, logs.stream().filter(log -> log.equals("PrototypeDependentWithProxy")).count());
    }

    @Test
    void should_return_twice_prototype_proxy_when_invoke_prototype_toString_method() {
        SingletonDependsOnPrototypeProxyBatchCall bean = context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);
        bean.getPrototype().toString();
        bean.getPrototype().toString();
        List<String> logs = bean.getLogger().getLogs();
        assertEquals(2, logs.stream().filter(log -> log.equals("PrototypeDependentWithProxy")).count());
    }

    @Test
    void should_return_zero_prototype_when_invoke_singleton_twice() {
        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);
        context.getBean(SingletonDependsOnPrototypeProxy.class);
        List <String> logs = bean.getLogger().getLogs();
        assertEquals(0, logs.stream().filter(log -> log.equals("PrototypeDependentWithProxy")).count());
    }
}
