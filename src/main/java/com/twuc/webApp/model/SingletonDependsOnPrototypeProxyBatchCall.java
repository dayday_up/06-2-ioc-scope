package com.twuc.webApp.model;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxyBatchCall {
    private final PrototypeDependentWithProxy prototype;

    private final MyLogger logger;

    public SingletonDependsOnPrototypeProxyBatchCall(PrototypeDependentWithProxy prototype, MyLogger logger) {
        this.prototype = prototype;
        this.logger = logger;
    }

    public MyLogger getLogger() {
        return logger;
    }

    public PrototypeDependentWithProxy getPrototype() {
        return prototype;
    }
}
