package com.twuc.webApp.model;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxy {
    private final PrototypeDependentWithProxy prototype;

    private final MyLogger logger;

    public SingletonDependsOnPrototypeProxy(PrototypeDependentWithProxy prototype, MyLogger logger) {
        this.prototype = prototype;
        this.logger = logger;
    }

    public void invokeMethod() {
        this.prototype.getLogger();
    }

    public PrototypeDependentWithProxy getPrototype() {
        return prototype;
    }

    public MyLogger getLogger() {
        return logger;
    }

}
