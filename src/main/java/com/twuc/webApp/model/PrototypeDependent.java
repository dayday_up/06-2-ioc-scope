package com.twuc.webApp.model;

import org.springframework.stereotype.Component;

@Component
public class PrototypeDependent {
    private final MyLogger myLogger;

    public PrototypeDependent(MyLogger myLogger) {
        myLogger.log("prototypeDependent()");
        this.myLogger = myLogger;
    }
}
