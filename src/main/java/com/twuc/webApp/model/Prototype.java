package com.twuc.webApp.model;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class Prototype {
    private final MyLogger logger;

    public Prototype(MyLogger logger) {
        this.logger = logger;
        logger.log("prototype()");
    }

    public MyLogger getLogger() {
        return logger;
    }
}
