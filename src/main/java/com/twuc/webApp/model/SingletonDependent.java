package com.twuc.webApp.model;
import org.springframework.stereotype.Component;

@Component
public class SingletonDependent {
    private final MyLogger myLogger;

    public SingletonDependent(MyLogger myLogger) {
        myLogger.log("singleton");
        this.myLogger = myLogger;
    }

    public MyLogger getMyLogger() {
        return myLogger;
    }
}
