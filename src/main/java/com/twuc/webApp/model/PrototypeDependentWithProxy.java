package com.twuc.webApp.model;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PrototypeDependentWithProxy {
    private final MyLogger logger;

    public PrototypeDependentWithProxy(MyLogger logger) {
        this.logger = logger;
        logger.log("PrototypeDependentWithProxy");
    }

    public MyLogger getLogger() {
        return logger;
    }
}
