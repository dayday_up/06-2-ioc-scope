package com.twuc.webApp.model;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MyLogger {
    private final List<String> logs = new ArrayList<>();

    public void log(String log) {
        this.logs.add(log);
    }

    public List<String> getLogs() {
        return logs;
    }
}
