package com.twuc.webApp.model;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototype {
    private final MyLogger myLogger;

    public MyLogger getMyLogger() {
        return myLogger;
    }

    public SingletonDependsOnPrototype(MyLogger myLogger, PrototypeDependent prototypeDependent) {
        myLogger.log("singletonDependsOnPrototype()");
        this.prototypeDependent = prototypeDependent;
        this.myLogger = myLogger;
    }

    private PrototypeDependent prototypeDependent;
}
