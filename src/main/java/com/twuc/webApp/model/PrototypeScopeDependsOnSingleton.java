package com.twuc.webApp.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PrototypeScopeDependsOnSingleton {
    private SingletonDependent singletonDependent;
    private final MyLogger myLogger;

    public PrototypeScopeDependsOnSingleton(MyLogger myLogger) {
        myLogger.log("prototype");
        this.myLogger = myLogger;
    }

    public MyLogger getMyLogger() {
        return myLogger;
    }
}
